﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mrs_MagazaBMAtamaBakimTablosu
{
    class Program
    {
        static void Main(string[] args)
        {
            ProgramRunOne();//Bölge Müdürü Dağılım güncellemesi için.

            ProgramRunTwo();//Bölge Satış Müdürü Dağılım güncellemesi için... Mimari İşler Ticket projesi geliştirmesi.

        }


        private static void ProgramRunOne()
        {
            DataTable dtIKOnayci = Veri.veriAl(@ConfigurationManager.AppSettings["sqlIKOnayciSorgu"]);

            DataTable dtMagazaBM = Veri.veriAl(@ConfigurationManager.AppSettings["sqlMagazaBMSorgu"]);
            try
            {
                foreach (DataRow rowIKOnayci in dtIKOnayci.Rows)
                {
                    //rowIKOnayci["cmbMagaza"]
                    if (rowIKOnayci["cmbMagaza"].ToString() != "")
                    {
                        DataRow[] rowMagazaBM = dtMagazaBM.Select("MagazaDepartmanId =" + rowIKOnayci["cmbMagaza"].ToString());

                        if (rowMagazaBM.Count() > 0)
                        {
                            if (rowMagazaBM[0]["BolgeMuduruSicil"].ToString() != rowIKOnayci["txtBolgeYonUserId"].ToString())
                            {
                                //Güncelleme işlemi yapılacak.
                                using (SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["sqlBaglanti"]))
                                {
                                    con.Open();
                                    SqlCommand cmd = new SqlCommand();
                                    cmd.Connection = con;

                                    cmd.CommandText = "UPDATE E_mrs_IstenCikisSureciGuncel_M_IKOnay_Magazalar SET txtBolgeYonUserId = @BMSicilNo, txtBolgeYoneticisiBilgileri = @BMAdi WHERE cmbMagaza = @magazaSicil ";
                                    cmd.Parameters.AddWithValue("@BMSicilNo", rowMagazaBM[0]["BolgeMuduruSicil"].ToString());
                                    cmd.Parameters.AddWithValue("@BMAdi", rowMagazaBM[0]["BolgeMuduruAdi"].ToString());
                                    cmd.Parameters.AddWithValue("@magazaSicil", rowIKOnayci["cmbMagaza"].ToString());

                                    cmd.ExecuteNonQuery();
                                    cmd.Dispose();

                                    con.Close();
                                    con.Dispose();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.logYaz("Hata: " + ex.Message.ToString());
                Log.logYaz("Hata: " + ex.StackTrace.ToString());
            }

            Log.logYaz("İşlem Tamamlandı. İnsan Kaynakları için Bölge Müdürü bazlı...");
 
        }



        private static void ProgramRunTwo()
        {
            DataTable dtMIOnayci = Veri.veriAl(@ConfigurationManager.AppSettings["sqlMimariIslerOnayciSorgu"]);

            DataTable dtMagazaBSM = Veri.veriAl(@ConfigurationManager.AppSettings["sqlMagazaBolgeSatisMuduruSorgu"]);
            try
            {
                foreach (DataRow rowMIOnayci in dtMIOnayci.Rows)
                {
                    //rowIKOnayci["cmbMagaza"]
                    if (rowMIOnayci["cmbMagaza"].ToString() != "")
                    {
                        DataRow[] rowMagazaBSM = dtMagazaBSM.Select("MagazaDepartmanId =" + rowMIOnayci["cmbMagaza"].ToString());

                        if (rowMagazaBSM.Count() > 0)
                        {
                            if (rowMagazaBSM[0]["BolgeSatisMuduruSicil"].ToString() != rowMIOnayci["txtSatisYonUserId"].ToString())
                            {
                                //Güncelleme işlemi yapılacak.
                                using (SqlConnection con = new SqlConnection(ConfigurationManager.AppSettings["sqlBaglanti"]))
                                {
                                    con.Open();
                                    SqlCommand cmd = new SqlCommand();
                                    cmd.Connection = con;

                                    cmd.CommandText = "UPDATE E_mrs_Ticket_Sureci_MI_M_MIOnay_Magazalar SET txtSatisYonUserId = @BMSicilNo, txtSatisYoneticisiBilgileri = @BMAdi WHERE cmbMagaza = @magazaSicil ";
                                    cmd.Parameters.AddWithValue("@BMSicilNo", rowMagazaBSM[0]["BolgeSatisMuduruSicil"].ToString());
                                    cmd.Parameters.AddWithValue("@BMAdi", rowMagazaBSM[0]["BolgeSatisMuduruAdi"].ToString());
                                    cmd.Parameters.AddWithValue("@magazaSicil", rowMIOnayci["cmbMagaza"].ToString());

                                    cmd.ExecuteNonQuery();
                                    cmd.Dispose();

                                    con.Close();
                                    con.Dispose();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.logYaz("Hata: " + ex.Message.ToString());
                Log.logYaz("Hata: " + ex.StackTrace.ToString());
            }

            Log.logYaz("İşlem Tamamlandı. Mimari İşler içi Satış Müdürü Bazlı");

        }
    }
}
