﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mrs_MagazaBMAtamaBakimTablosu
{
    class Veri
    {
        public static DataTable veriAl(string sqlSorgu)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["sqlBaglanti"]);
            try
            {
                conn.Open();
                SqlDataAdapter adpt = new SqlDataAdapter(sqlSorgu, conn);
                DataTable dataTable = new DataTable();
                adpt.Fill(dataTable);
                return dataTable;
            }
            catch (Exception ex)
            {
                Log.logYaz("Hata: " + ex.Message.ToString());
                Log.logYaz("Hata: " + ex.StackTrace.ToString());
                throw new Exception("Datatable alınamadı !! " + ex.Message);
            }
            finally
            {
                conn.Close();
            }
        }
    }
}
