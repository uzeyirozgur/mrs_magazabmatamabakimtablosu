﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mrs_MagazaBMAtamaBakimTablosu
{
    class Log
    {
        public static void logYaz(string mesaj)
        {

            string logKlasorYolu = Path.Combine(Environment.CurrentDirectory, @ConfigurationManager.AppSettings["logKlasorAdi"] + @"\");
            string logDosyaYolu = DateTime.Now.ToString(ConfigurationManager.AppSettings["logDosyaTarihFormat"]) + @ConfigurationManager.AppSettings["logDosyaAdi"] + ".txt";
            string logYolu = logKlasorYolu + logDosyaYolu;
            if (!Directory.Exists(logKlasorYolu))
            {
                Directory.CreateDirectory(logKlasorYolu);
            }
            StreamWriter sw = File.AppendText(logYolu);
            try
            {
                string logLine = string.Format(@"{0:G}: {1}.", DateTime.Now, mesaj);
                sw.WriteLine(logLine);
                Console.WriteLine(logLine);
                if (ConfigurationManager.AppSettings["logDebug"] == "1")
                {
                    Console.ReadKey();
                }

            }
            finally
            {
                sw.Close();
            }
        }
    }
}
